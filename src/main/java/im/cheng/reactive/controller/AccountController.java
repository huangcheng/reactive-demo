package im.cheng.reactive.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

import im.cheng.reactive.Response;
import im.cheng.reactive.model.AccountModel;
import im.cheng.reactive.repository.AccountRepository;

@RestController
public class AccountController {
    private AccountRepository accountRepository;

    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/account")
    public Mono<Response<AccountModel>> account(@RequestParam("uid") int uid) {
        return accountRepository
                .findById(uid)
                .map(account -> new Response<>(0, "success", account))
                .onErrorResume(e -> Mono.just(new Response<>(1, e.getMessage(), null)));
    }
}
