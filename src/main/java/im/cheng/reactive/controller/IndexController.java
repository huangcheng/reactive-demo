package im.cheng.reactive.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/observable")
    public String observable() {
        return "observable";
    }

    @GetMapping("/operators")
    public String operators() {
        return "operators";
    }

    @GetMapping("/advance")
    public String advance() {
        return "advance";
    }
}
