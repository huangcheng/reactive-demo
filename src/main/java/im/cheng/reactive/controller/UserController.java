package im.cheng.reactive.controller;

import im.cheng.reactive.Response;
import im.cheng.reactive.repository.UserRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.datafaker.Faker;

import im.cheng.reactive.model.UserModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Stream;

@RestController
public class UserController {
    private final UserRepository userRepository;

    UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public Mono<Response<UserModel[]>> user(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        return userRepository.findAll(page - 1, size).collectList().map(users -> new Response<>(0, "success", users.toArray(new UserModel[0])));
    }
}
