package im.cheng.reactive.controller;

import java.time.Duration;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import im.cheng.reactive.Response;
import im.cheng.reactive.model.BillingModel;
import im.cheng.reactive.repository.BillingRepository;

@RestController
public class BillingController {
    private final BillingRepository billingRepository;

    public BillingController(BillingRepository billingRepository) {
        this.billingRepository = billingRepository;
    }

    @GetMapping("/billing")
    public Mono<Response<BillingModel>> findById(@RequestParam("uid") int uid) throws InterruptedException {
        Random random = new Random();

        return billingRepository
                .findById(uid)
                .delayElement(Duration.ofSeconds(random.nextLong(1, 6)))
                .map(billing -> new Response<>(0, "success", billing));
    }
}
