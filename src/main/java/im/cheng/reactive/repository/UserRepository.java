package im.cheng.reactive.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;


import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;

import net.datafaker.Faker;

import im.cheng.reactive.model.UserModel;

@Repository
public class UserRepository {
    private UserModel[] users;

    public UserRepository() {
        Faker faker = new Faker();
        users = Stream
                .generate(new Random()::nextInt)
                .limit(100)
                .map(Math::abs)
                .map(i -> new UserModel(
                        i,
                        faker.name().firstName(),
                        faker.name().lastName(),
                        faker.name().username(),
                        faker.internet().emailAddress(),
                        faker.address().fullAddress(),
                        faker.phoneNumber().cellPhone()
                        )
                ).toArray(UserModel[]::new);
    }

    public Flux<UserModel> findAll(int page, int size) {
        return Flux.fromArray(users).skip(page * size).take(size);
    }
}
