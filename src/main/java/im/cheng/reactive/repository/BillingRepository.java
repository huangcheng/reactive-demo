package im.cheng.reactive.repository;

import im.cheng.reactive.model.BillingModel;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Random;

@Repository
public class BillingRepository {
    public Mono<BillingModel> findById(int uid) {
        Random random = new Random();

        return Mono.just(new BillingModel(uid, random.nextDouble(), random.nextDouble() * random.nextInt() * 1000));
    }
}
