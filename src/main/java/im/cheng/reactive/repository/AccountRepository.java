package im.cheng.reactive.repository;

import java.util.Random;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import im.cheng.reactive.model.AccountModel;

import javax.management.RuntimeErrorException;

@Repository
public class AccountRepository {
    public Mono<AccountModel> findById(int uid) {
        Random random = new Random();

        return Mono.just(uid)
                .flatMap(id ->
                        id == 0 ? Mono.error(new RuntimeException("uid can't be 0!")) : Mono.just(new AccountModel(uid, random.nextBoolean(), random.nextBoolean()))
                );
    }
}
