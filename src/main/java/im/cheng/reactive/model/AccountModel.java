package im.cheng.reactive.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountModel {
    private int uid;
    private boolean isActive;
    private boolean isAdmin;
}
