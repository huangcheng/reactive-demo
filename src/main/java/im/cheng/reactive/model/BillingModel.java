package im.cheng.reactive.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BillingModel {
    private int uid;
    private Double coupon;
    private Double total;
}
