package im.cheng.reactive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class UserModel {
    private int id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private String address;
    private String phone;
}
